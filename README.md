MySQL vs PostgreSQL tests
=========================

Latvijas Biznesa Koledža, DTK5 studenta Oļega Čapligina kvalifikācijas darbs

Paskaidrojums
-------------

Projekta testi realizēti kā CLI aplikācijas un ar viņiem ir jāstrādā komandrindā. 
Tas ļauj samazināt svešo faktoru ietekmi uz rezultātiem.  Ar šī projekta koda bāzi 
izstrādes gaitu var iepazīties šī GIT repozitorijā  


Izmantošana
-----------

     # Linux
     ./yiic write/mysql # Ierakstīšanas tests priekš MySQL datubāzi
     ./yiic write/pgsql # Ierakstīšanas tests priekš PostgreSQL datubāzi
     ./yiic test/mysql  # Darba emulācijas tests priekš MySQL datubāzi
     ./yiic test/pgsql  # Darba emulācijas tests priekš PostgreSQL datubāzi
     # Windows
     ./yii.bat write/mysql # Ierakstīšanas tests priekš MySQL datubāzi
     ./yii.bat write/pgsql # Ierakstīšanas tests priekš PostgreSQL datubāzi
     ./yii.bat test/mysql  # Darba emulācijas tests priekš MySQL datubāzi
     ./yii.bat test/pgsql  # Darba emulācijas tests priekš PostgreSQL datubāzi


Copyright
---------

Oļegs Čapligins, 
Latvijas Biznesa Koledža, 
oleg@dautkom.lv, 
+37127879024