<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\Console;

/**
 * Truncates data in databases
 * @package app\commands
 */
class FlushController extends Controller
{

    /**
     * Truncate data in all databases
     */
    public function actionIndex()
    {
        $this->actionMysql();
        $this->actionPgsql();
    }

    /**
     * Truncates data in MySQL databases
     */
    public function actionMysql()
    {
        \Yii::$app->db->createCommand()->truncateTable('{{counttable}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{linktable}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{nodetable}}')->execute();
        Console::stdout("MySQL tables truncated\n");
    }

    /**
     * Truncates data in PostgreSQL databases
     */
    public function actionPgsql()
    {
        \Yii::$app->pg->createCommand()->truncateTable('{{counttable}}')->execute();
        \Yii::$app->pg->createCommand()->truncateTable('{{linktable}}')->execute();
        \Yii::$app->pg->createCommand()->truncateTable('{{nodetable}}')->execute();
        \Yii::$app->pg->createCommand("ALTER SEQUENCE nodetable_id_seq RESTART WITH 1;")->execute();
        Console::stdout("PostgreSQL tables truncated\n");
    }

}
