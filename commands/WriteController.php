<?php

namespace app\commands;

use app\components\Collection;
use app\models\Counttable;
use app\models\Linktable;
use app\models\Nodetable;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Commands for filling databases with default datasets
 * @package app\commands
 */
class WriteController extends Controller
{

    /**
     * @var int
     */
    private $size;

    /**
     * @var float
     */
    private $start;

    /**
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->size  = \Yii::$app->params['dataset'];
        $this->start = microtime(true);
    }

    /**
     * Fills MySQL database with default dataset
     */
    public function actionMysql()
    {
        $this->writeDataset();
    }

    /**
     * Fills PostgreSQL database with default dataset
     */
    public function actionPgsql()
    {
        $this->writeDataset('pg');
    }

    /**
     * @param string $mode Database type 'my' or 'pg'
     */
    private function writeDataset($mode = 'my')
    {

        for($i=0; $i<$this->size; $i++) {

            $d = Collection::generateDataset();
            $n = new Nodetable();
            $c = new Counttable();
            $l = new Linktable();

            if ($mode == 'pg') {
                $l::setDb($mode);
                $n::setDb($mode);
                $c::setDb($mode);
            }

            $c->setAttributes($d);
            $c->setAttribute('id', $d['id2']);
            $c->save();

            $n->setAttributes($d);
            if( $mode == 'my' ) {
                $n->setAttribute('id', null);
            }
            $n->save();

            $l->setAttributes($d);
            $l->setAttribute('data', $d['ldata']);
            $l->setAttribute('id1', $n->id);
            if( $mode == 'pg' ) {
                $d = \Yii::$app->pg->createCommand("SELECT currval(pg_get_serial_sequence('nodetable','id'));")->queryScalar();
                $l->setAttribute('id1', $d);
            }
            $l->save();

        }

        $end   = microtime(true);
        $total = round($end - $this->start, 2);

        Console::stdout(ucfirst($mode)."SQL dataset created in $total seconds");

    }

}
