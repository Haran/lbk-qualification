<?php

namespace app\commands;

use app\components\Collection;
use app\models\Counttable;
use app\models\Nodetable;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\Console;

/**
 * Commands for emulating real workload on databases
 * Select 7
 * Insert 1
 * Update 1
 * Delete 1
 * @package app\commands
 */
class TestController extends Controller
{

    /**
     * @var string
     */
    public $defaultAction = 'mysql';

    /**
     * @var int
     */
    private $size;

    /**
     * @var float
     */
    private $start;

    /**
     * @var int
     */
    private $select = 0;

    /**
     * @var int
     */
    private $update = 0;

    /**
     * @var int
     */
    private $insert = 0;

    /**
     * @var int
     */
    private $delete = 0;

    /**
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->size  = \Yii::$app->params['dataset']+1;
        $this->start = microtime(true);
    }

    /**
     * Test MySQL database emulating the real workload
     */
    public function actionMysql()
    {
        
        for( $i=1; $i<$this->size; $i++ ) {

            // UPDATE
            if ($i > 0 && $i % 5 == 0) {

                $d       = Collection::generateDataset();
                $u       = Nodetable::findOne($i);
                $u->data = $d['data'];

                if ($u->save()) {
                    $this->update++;
                }

            }
            // INSERT
            elseif ($i > 0 && $i % 7 == 0) {

                $d = Collection::generateDataset();
                $c = new Counttable();

                $c->setAttributes($d);
                $c->setAttribute('id', $d['id2']);

                if($c->save()) {
                    $this->insert++;
                }
            }

            // DELETE
            elseif ($i > 0 && $i % 8 == 0) {
                if( Nodetable::findOne($i)->delete() ) {
                    $this->delete++;
                }
            }
            // SELECT
            else {
                $rows = (new Query())
                    ->select('*')
                    ->from('nodetable n')
                    ->leftJoin('linktable l', 'n.id = l.id1')
                    ->leftJoin('counttable c', 'l.id2 = c.id')
                    ->where(['n.id' => $i])
                    ->all()
                ;
                unset($rows);
                $this->select++;
            }

        }

        $end   = microtime(true);
        $total = round($end - $this->start, 2);

        Console::stdout("MySQL test finished in $total seconds\n");
        Console::stdout("\tSELECTS: {$this->select}\n");
        Console::stdout("\tUPDATES: {$this->update}\n");
        Console::stdout("\tINSERTS: {$this->insert}\n");
        Console::stdout("\tDELETES: {$this->delete}\n");
        
    }

    /**
     * Test PostgreSQL database emulating the real workload
     */
    public function actionPgsql()
    {

        for( $i=1; $i<$this->size; $i++ ) {

            // UPDATE
            if ($i > 0 && $i % 5 == 0) {

                $d       = Collection::generateDataset();
                $u       = new Nodetable();
                $z       = $u::find()->where(['id' => $i])->one(\Yii::$app->pg);
                $z->data = $d['data'];

                if ($z->save()) {
                    $this->update++;
                }

            }
            // INSERT
            elseif ($i > 0 && $i % 7 == 0) {

                $sql = \Yii::$app->pg->createCommand()->insert('counttable', [
                    'id'        => mt_rand(1, 2000000000),
                    'link_type' => mt_rand(1, 2000000000),
                    'count'     => mt_rand(1, 2000000000),
                    'time'      => mt_rand(1, 2000000000),
                    'version'   => mt_rand(1, 2000000000),
                ])->execute();

                if ($sql) {
                    $this->insert++;
                }

            }

            // DELETE
            elseif ($i > 0 && $i % 8 == 0) {
                if( \Yii::$app->pg->createCommand()->delete('nodetable', ['id' => $i])->execute() ) {
                    $this->delete++;
                }
            }
            // SELECT
            else {
                $rows = (new Query())
                    ->select('*')
                    ->from('public.nodetable n')
                    ->leftJoin('public.linktable l', 'n.id = l.id1')
                    ->leftJoin('public.counttable c', 'l.id2 = c.id')
                    ->where(['n.id' => $i])
                    ->all(\Yii::$app->pg)
                ;
                unset($rows);
                $this->select++;
            }

        }

        $end   = microtime(true);
        $total = round($end - $this->start, 2);

        Console::stdout("PgSQL test finished in $total seconds\n");
        Console::stdout("\tSELECTS: {$this->select}\n");
        Console::stdout("\tUPDATES: {$this->update}\n");
        Console::stdout("\tINSERTS: {$this->insert}\n");
        Console::stdout("\tDELETES: {$this->delete}\n");

    }

}
