-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2016 at 09:51 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `counttable`
--

CREATE TABLE `counttable` (
  `id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `link_type` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `version` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `linktable`
--

CREATE TABLE `linktable` (
  `id1` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `id2` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `link_type` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `visibility` tinyint(3) NOT NULL DEFAULT '0',
  `data` varchar(255) NOT NULL DEFAULT '',
  `time` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1
PARTITION BY KEY (id1)
PARTITIONS 16;

-- --------------------------------------------------------

--
-- Table structure for table `nodetable`
--

CREATE TABLE `nodetable` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `version` bigint(20) UNSIGNED NOT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  `data` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `counttable`
--
ALTER TABLE `counttable`
  ADD PRIMARY KEY (`id`,`link_type`);

--
-- Indexes for table `linktable`
--
ALTER TABLE `linktable`
  ADD PRIMARY KEY (`link_type`,`id1`,`id2`),
  ADD KEY `id1_type` (`id1`,`link_type`,`visibility`,`time`,`id2`,`version`,`data`);

--
-- Indexes for table `nodetable`
--
ALTER TABLE `nodetable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nodetable`
--
ALTER TABLE `nodetable`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10001;
