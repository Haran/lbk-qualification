-- noinspection SqlResolveForFile
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-05-25 11:18:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2127 (class 1262 OID 16713)
-- Name: test; Type: DATABASE; Schema: -; Owner: test
--

CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE test OWNER TO test;

\connect test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 183 (class 1259 OID 16742)
-- Name: counttable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE counttable (
    id bigint DEFAULT 0 NOT NULL,
    link_type bigint DEFAULT 0 NOT NULL,
    count integer DEFAULT 0 NOT NULL,
    "time" bigint DEFAULT 0 NOT NULL,
    version bigint DEFAULT 0 NOT NULL
);


ALTER TABLE counttable OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16730)
-- Name: linktable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE linktable (
    id1 bigint DEFAULT 0 NOT NULL,
    id2 bigint DEFAULT 0 NOT NULL,
    link_type bigint DEFAULT 0 NOT NULL,
    visibility smallint DEFAULT 0 NOT NULL,
    data character varying(255) NOT NULL,
    "time" bigint DEFAULT 0 NOT NULL,
    version integer DEFAULT 0 NOT NULL
);


ALTER TABLE linktable OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16714)
-- Name: nodetable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE nodetable (
    id integer NOT NULL,
    type integer NOT NULL,
    version bigint NOT NULL,
    "time" integer NOT NULL,
    data text NOT NULL
);


ALTER TABLE nodetable OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16753)
-- Name: nodetable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE nodetable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nodetable_id_seq OWNER TO postgres;

--
-- TOC entry 2131 (class 0 OID 0)
-- Dependencies: 184
-- Name: nodetable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE nodetable_id_seq OWNED BY nodetable.id;


--
-- TOC entry 1990 (class 2604 OID 16755)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nodetable ALTER COLUMN id SET DEFAULT nextval('nodetable_id_seq'::regclass);


--
-- TOC entry 2008 (class 2606 OID 16751)
-- Name: counttable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY counttable
    ADD CONSTRAINT counttable_pkey PRIMARY KEY (id, link_type);


--
-- TOC entry 2006 (class 2606 OID 16740)
-- Name: linktable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY linktable
    ADD CONSTRAINT linktable_pkey PRIMARY KEY (id1, id2, link_type);


--
-- TOC entry 2003 (class 2606 OID 16937)
-- Name: nodetable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nodetable
    ADD CONSTRAINT nodetable_pkey PRIMARY KEY (id);


--
-- TOC entry 2004 (class 1259 OID 16752)
-- Name: id1_type; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX id1_type ON linktable USING btree (id1, link_type, visibility, "time", id2, version, data);


--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-25 11:18:49

--
-- PostgreSQL database dump complete
--
