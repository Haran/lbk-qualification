<?php

$config = [
    
    'id'                  => 'lbk-qualification-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'app\commands',
    
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'mysql:host=localhost;dbname=test',
            'username' => 'test',
            'password' => 'test',
            'charset'  => 'utf8',
        ],
        'pg' => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'pgsql:host=localhost;port=5432;dbname=test',
            'username' => 'postgres',
            'password' => '',
            'charset'  => 'utf8',
        ],
    ],
    
    'params' => [
        'adminEmail' => 'oleg@dautkom.lv',
        'dataset'    => 10000,
    ],
    
];

return $config;
