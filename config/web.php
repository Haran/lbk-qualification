<?php

$config = [

    'id'        => 'lbk-qualification-web',
    'basePath'  => dirname(__DIR__),
    'bootstrap' => ['log'],

    'components' => [
        'request' => [
            'cookieValidationKey' => 'btAXnZB3HNtfDgpf2uaEzon0QDXYSB4d',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    
    'params' => [
        'adminEmail' => 'oleg@dautkom.lv',
    ],
    
];

if (YII_ENV_DEV) {
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = ['class' => 'yii\debug\Module'];
    $config['bootstrap'][]      = 'gii';
    $config['modules']['gii']   = ['class' => 'yii\gii\Module'];
}

return $config;
