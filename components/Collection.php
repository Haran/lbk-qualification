<?php

namespace app\components;

class Collection
{

    /**
     * @return array
     */
    public static function generateDataset()
    {
        return [
            'id2'       => mt_rand(1, 2000000000),
            'count'     => mt_rand(1, 2000000000),
            'type'      => mt_rand(1, 2000000000),
            'link_type' => mt_rand(1, 2000000000),
            'version'   => mt_rand(1, 2000000000),
            'time'      => mt_rand(1, 2000000000),
            'data'      => \Yii::$app->getSecurity()->generateRandomString(1024),
            'ldata'     => \Yii::$app->getSecurity()->generateRandomString(250),
        ];
    }

}