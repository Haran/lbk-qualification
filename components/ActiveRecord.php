<?php

namespace app\components;

use yii;

class ActiveRecord extends yii\db\ActiveRecord
{

    /**
     * @var string
     */
    private static $db = null;

    /**
     * @param string $db
     */
    public static function setDb($db)
    {
        self::$db = $db;
    }

    /**
     * @throws yii\base\InvalidConfigException
     * @return null|object|yii\db\Connection
     */
    public static function getDb()
    {
        return is_null(self::$db) ? Yii::$app->getDb() : Yii::$app->get(self::$db);
    }

}
