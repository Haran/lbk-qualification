<?php

namespace app\models;

use app\components\ActiveRecord;
use yii;

/**
 * This is the model class for table "linktable".
 *
 * @property string $id1
 * @property string $id2
 * @property string $link_type
 * @property integer $visibility
 * @property string $data
 * @property string $time
 * @property integer $version
 */
class Linktable extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'linktable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id1', 'id2', 'link_type'], 'required'],
            [['id1', 'id2', 'link_type', 'visibility', 'time', 'version'], 'integer'],
            [['data'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id1'        => 'Id1',
            'id2'        => 'Id2',
            'link_type'  => 'Link Type',
            'visibility' => 'Visibility',
            'data'       => 'Data',
            'time'       => 'Time',
            'version'    => 'Version',
        ];
    }
}
