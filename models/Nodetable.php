<?php

namespace app\models;

use app\components\ActiveRecord;
use yii;

/**
 * This is the model class for table "nodetable".
 *
 * @property string $id
 * @property integer $type
 * @property string $version
 * @property integer $time
 * @property string $data
 */
class Nodetable extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nodetable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'version', 'time', 'data'], 'required'],
            [['type', 'version', 'time'], 'integer'],
            [['data'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'type'    => 'Type',
            'version' => 'Version',
            'time'    => 'Time',
            'data'    => 'Data',
        ];
    }
}
