<?php

namespace app\models;

use yii;
use app\components\ActiveRecord;

/**
 * This is the model class for table "counttable".
 *
 * @property string $id
 * @property string $link_type
 * @property integer $count
 * @property string $time
 * @property string $version
 */
class Counttable extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'counttable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'link_type'], 'required'],
            [['id', 'link_type', 'count', 'time', 'version'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'link_type' => 'Link Type',
            'count'     => 'Count',
            'time'      => 'Time',
            'version'   => 'Version',
        ];
    }
    
}
