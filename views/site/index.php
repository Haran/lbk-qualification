<?php
/* @var $this yii\web\View */
$this->title = 'Olegs Capligins, LBK, DTK5';
?>

<div class="jumbotron">
    <h1>Labdien!</h1>
    <p class="lead">Tā ir vienīga web-lapa kvalifikācijas darbam</p>
</div>

<div class="body-content">

    <div class="row">
        <div class="col-lg-12 text-justify">
            <h2>Paskaidrojums</h2>
            <p>
                Projekta testi realizēti kā CLI aplikācijas un ar viņiem ir jāstrādā komandrindā. Tas ļauj samazināt
                svešo faktoru ietekmi uz rezultātiem. Ar šī projekta koda bāzi izstrādes gaitu var iepazīties GIT
                repozitorijā uz Bitbucket serveriem: <a href="https://bitbucket.org/Haran/lbk-qualification" target="_blank">
                https://bitbucket.org/Haran/lbk-qualification</a>.
            </p>
            <h2>Izmantošana</h2>
                <div class="well well-sm" style="font-family: 'Courier New', monospace">
                # Linux<br>
                <code>./yiic write/mysql</code>  # Ierakstīšanas tests priekš MySQL datubāzi<br>
                <code>./yiic write/pgsql</code>  # Ierakstīšanas tests priekš PostgreSQL datubāzi<br>
                <code>./yiic test/mysql</code>   &nbsp;# Darba emulācijas tests priekš MySQL datubāzi<br>
                <code>./yiic test/pgsql</code>   &nbsp;# Darba emulācijas tests priekš PostgreSQL datubāzi<br>
                <hr>
                # Windows<br>
                <code>./yii.bat write/mysql</code>  # Ierakstīšanas tests priekš MySQL datubāzi<br>
                <code>./yii.bat write/pgsql</code>  # Ierakstīšanas tests priekš PostgreSQL datubāzi<br>
                <code>./yii.bat test/mysql</code>   &nbsp;# Darba emulācijas tests priekš MySQL datubāzi<br>
                <code>./yii.bat test/pgsql</code>   &nbsp;# Darba emulācijas tests priekš PostgreSQL datubāzi<br>
                </div>
            <h2>Autors</h2>
            <p>
                Oļegs Čapligins <br>
                Latvijas Biznesa Koledža, DTK5 <br>
                <a href="mailto:oleg@dautkom.lv">oleg@dautkom.lv</a><br>
                +371 27879024 <br>
            </p>
        </div>
    </div>

</div>
